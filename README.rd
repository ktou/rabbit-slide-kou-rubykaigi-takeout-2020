= Goodbye fat gem

Fat gem mechanism is useful to install extension library without any
compiler. Fat gem mechanism is especially helpful for Windows rubyists
because Windows rubyists don't have compiler. But there are some
downsides. For example, fat gem users can't use Ruby 2.7 (the latest
Ruby) until fat gem developers release a new gem for Ruby 2.7. As of
2020, pros of fat gem mechanism is decreasing and cons of it is
increasing. This talk describes the details of pros and cons of it then
says thanks and goodbye to fat gem.

== License

=== Slide

CC BY-SA 4.0

Use the followings for notation of the author:

  * Sutou Kouhei

==== ClearCode Inc. logo

CC BY-SA 4.0

Author: ClearCode Inc.

It is used in page header and some pages in the slide.

== For author

=== Show

  rake

=== Publish

  rake publish

== For viewers

=== Install

  gem install rabbit-slide-kou-rubykaigi-takeout-2020

=== Show

  rabbit rabbit-slide-kou-rubykaigi-takeout-2020.gem
